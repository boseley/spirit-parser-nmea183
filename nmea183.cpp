#include <boost/config/warning_disable.hpp>

#include <boost/spirit/home/x3.hpp>
#include <boost/spirit/home/x3/support/ast/variant.hpp>

#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/fusion/adapted/struct/define_struct.hpp>
#include <boost/fusion/include/define_struct.hpp>

#include <boost/fusion/sequence/io.hpp>
#include <boost/fusion/include/io.hpp>

#include <boost/optional.hpp>
#include <boost/optional/optional_io.hpp>

#include <boost/type_index.hpp>

#include <iostream>
#include <string>
#include <complex>
#include <cmath>

BOOST_FUSION_DEFINE_STRUCT(
    (nmea) (msg), vtg,
    (double, true_track)
    (char,   true_track_ref)
    (double, mag_track)
    (char,   mag_track_ref)
    (double, ground_speed)
    (char,   ground_speed_ref)
)

BOOST_FUSION_DEFINE_STRUCT(
    (nmea) (msg), gga,
    (unsigned, time_tag)
    (double, latitude)
    (double, longitude)
    (unsigned, fix_quality)
    (unsigned, num_sv)
    (double, hdop)
    (double, altitude)
    (double, height)
    (boost::optional<double>, dgps_age)
    (boost::optional<unsigned>, dgps_base_id)
)

BOOST_FUSION_DEFINE_STRUCT(
    (nmea) (msg), gll,
    (double, latitude)
    (double, longitude)
    (unsigned, fix_time)
    (char, data_active)
)

BOOST_FUSION_DEFINE_STRUCT(
    (nmea) (msg), gst,
    (double, utc_time_of_gga)
    (double, range_std_dev)
    (double, semi_major_error_std_dev)
    (double, semi_minor_error_std_dev)
    (double, orientation_semi_major_error)
    (double, latitude_errror_std_dev)
    (double, longitude_errror_std_dev)
    (double, latitude_error_std_dev)
)

using boost::fusion::operator<<;


namespace nmea { namespace msg {
    using variant = boost::spirit::x3::variant< vtg, gga, gll, gst >;
} }

namespace nmea{ namespace parser{
    namespace x3 = boost::spirit::x3;
    using x3::double_;
    using x3::omit;
    using x3::char_;
    using x3::uint_;
    using x3::lexeme;
    using x3::lit;
    using x3::repeat;
   
    // convert degree-minute formating to degrees 
    auto const dms_convert = [](auto& x){ 
        auto v = _attr(x); 
        long degrees = v/100; 
        auto minutes = v - 100*degrees;
        v =  degrees + minutes / 60.0;
        _val(x) = degrees + minutes / 60.0;
    }; 
    x3::rule< class dms, double > const dms = "dms" ;
    auto const dms_def = double_ [ dms_convert ] ;
    BOOST_SPIRIT_DEFINE ( dms );
    
    x3::rule< class gga, msg::gga >  const gga = "gga";
    auto const gga_def =
                     lit("GPGGA") 
                     >> uint_
                     >> dms >> omit[ char_ ]
                     >> dms >> omit[ char_ ]
                     >> uint_
                     >> uint_
                     >> double_
                     >> double_  >> omit[ char_ ]
                     >> double_  >> omit[ char_ ]
                     >> -(double_ >> uint_ );
    
    BOOST_SPIRIT_DEFINE ( gga );
    
    x3::rule< class vtg, msg::vtg >  const vtg = "vtg";
    auto const vtg_def =
                     lit("VTG") 
                     >>  double_ >> char_
                     >>  double_ >> char_
                     >>  double_ >> char_;
    
    BOOST_SPIRIT_DEFINE ( vtg );
    
    x3::rule< class gll, msg::gll > const gll = "gll";
    auto const gll_def =
                     lit("GLL") 
                     >> dms >> omit [ char_ ]
                     >> dms >> omit [ char_ ]
                     >> uint_ 
                     >> char_;
    
    BOOST_SPIRIT_DEFINE ( gll );
    
    x3::rule< class gst, msg::gst > const gst = "gst";
    auto const gst_def = 
                    lit("GST") 
                    >> double_ 
                    >> double_ 
                    >> double_ 
                    >> double_ 
                    >> double_ 
                    >> double_ 
                    >> double_ 
                    >> double_ ;
                    
    BOOST_SPIRIT_DEFINE ( gst );
    
    template< typename Iterator >
    bool parse( Iterator first, Iterator last, nmea::msg::variant& msg )
    {
       auto nmea = ( gga | gll | vtg | gst);
                    
       return phrase_parse( first, last, nmea , lit( "," ), msg ); 
    }
}}

int
main()
{
    std::string str = "GPGGA,123519,4807.038,N,01131.000,E,1,08,0.9,545.4,M,46.9,M,,,";
    std::string::const_iterator iter = str.begin( );
    std::string::const_iterator end = str.end( );
 
    nmea::msg::variant nmea_msg;
    bool r = nmea::parser::parse( iter, end, nmea_msg );
    if( r )
    {
       std::cout << "Parsing succeeded\n";
       boost::apply_visitor( [](auto& v) { std::cout <<  v << std::endl; }, nmea_msg );
    }
    else
    {
       std::cout << "Parsing failed\n";
    }

    std::cout << "Bye... :-) \n\n";
    return 0;
}

